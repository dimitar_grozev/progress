# Progress

## A simple web app for sending emails to an endpoint and validating the response.

# Technologies
- ASP \.NET Core MVC
- javaScript / jQuery
- AJAX
- Bootstrap
- HTML5
- CSS3

---
![Homepage](images/home.png)