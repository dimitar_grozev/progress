﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Progress.Models;

namespace Progress.Controllers
{
	public class HomeController : Controller
	{
		private readonly IHttpClientFactory clientFactory;
		private readonly IConfiguration configuration;

		public HomeController(IHttpClientFactory clientFactory,IConfiguration configuration)
		{
			this.clientFactory = clientFactory;
			this.configuration = configuration;
		}

		public IActionResult Index()
		{
			return View();
		}

		public async Task<IActionResult> Email(User user)
		{
			if (!ModelState.IsValid)
			{
				return Json(-1);

			}
			try
			{
				var json = JsonConvert.SerializeObject(user);
				var data = new StringContent(json, Encoding.UTF8, "application/json");

				var url = configuration["Url"];
				using var client = this.clientFactory.CreateClient("email");
				var response = await client.PostAsync(url, data);

				while ((int)response.StatusCode != 200)
				{
					response = await client.PostAsync(url, data);
				}

				return Json(1);
			}
			catch
			{
				return Json(-1);
			}
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
