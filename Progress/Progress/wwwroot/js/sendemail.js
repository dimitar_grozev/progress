﻿$(function () {
	$(document).on('click', '#submit-btn', function (x) {
		var email = $('#user-email').val();
		console.log(email);
		console.log("In func");
		$.ajax({
			url: '/Home/Email/',
			data: { 'email': email },
			beforeSend: function (xhr, opts) {
				$('#submit-btn').prop('disabled', 'true');

				if (email.length == 0) {
					bootbox.alert({
						message: "Please insert an email!",
						centerVertical: true,
						backdrop: true
					});
					$('#submit-btn').prop('disabled', false);

					xhr.abort();
				}
				else if (!validateEmail(email)) {
					console.log("Invalid email");

					bootbox.alert({
						message: "Please insert a valid email!",
						centerVertical: true,
						backdrop: true
					});
					$('#submit-btn').prop('disabled', false);
					xhr.abort();
				}
			},
			type: 'POST',
			success: function (data) {
				if (data == 1) {
					console.log("success ");
					bootbox.alert({
						message: "Check your email!",
						centerVertical: true,
						backdrop: true
					});
				}
				else if (data == 0) {
					console.log("error");
					bootbox.alert({
						message: "Something went wrong! Try again later!",
						centerVertical: true,
						backdrop: true
					});
				}
				$('#submit-btn').prop('disabled', false);

			},
			error: function (data) {
				console.log("error" + " " + data);

			}
		})
	});

});

function validateEmail($email) {
	var emailReg = /^([\w-\.]+@@([\w-]+\.)+[\w-]{2,4})?$/gi;
	return emailReg.test($email);
}