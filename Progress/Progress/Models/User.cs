﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Progress.Models
{
	public class User
	{
		[Display(Name = "Email")]
		[RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email ex: johndoe@sth.else")]
		public string email { get; set; }
	}
}
